"""
noxfile.py Configuration file for Nox
"""
# pylint: disable=import-error
import os
import nox

nox.options.reuse_existing_virtualenvs = True
nox.options.sessions = ["lint", "tests"]


@nox.session
def lint(session):
    """ Runs linters and fixers """
    session.run("poetry", "install", external=True)
    session.run("black", "src")
    session.run("isort", "--profile", "black", "src")
    session.run("pylint", "src")


@nox.session
def tests(session):
    """ Runs tests """
    session.run("poetry", "install", external=True)
    session.run("coverage", "run", "--source", "src", "-m", "pytest", "-sv")
    session.run("coverage", "report")
    session.run("coverage", "xml")
    session.run("coverage", "html")


@nox.session
def release(session):
    """Build and release to a repository"""
    pypi_user: str = os.environ.get("PYPI_USER")
    pypi_pass: str = os.environ.get("PYPI_PASS")
    if not pypi_user or not pypi_pass:
        session.error(
            "Environment variables for release: PYPI_USER, PYPI_PASS are missing!",
        )
    session.run("poetry", "install", external=True)
    session.run("poetry", "build", external=True)
    session.run(
        "poetry",
        "publish",
        "-r",
        "testpypi",
        "-u",
        pypi_user,
        "-p",
        pypi_pass,
        external=True,
    )
